import React, { Component } from 'react';
import axios from "axios";

class App extends Component {
  state = {
    heading: "axios api call",
    items: [],
    loading: true,
  }


  fetchData() {
    axios.get(`	https://api.adviceslip.com/advice`)
      .then(response => {
        console.log(response);
        this.setState({
          items: [response.data.slip],
          loading: false
        })

      })

      .catch(error => {
        console.log(error);
      });
  }
  componentDidMount() {
    this.fetchData();
  }
  render() {
    if (this.state.loading) {
      return <h1>loading...</h1>
    }
    return (
      <>
        {this.state.heading}
        <div>
          {this.state.items.map(item => {
            return <div key={item.id}>
              <button onClick={() => { this.fetchData() }}>Free Advice</button>
              <h1>{item.advice}</h1>
            </div>
          })}
        </div>
      </>

    );
  }
}

export default App;